const express = require('express');
const app = express();

app.use(express.static('public'));


// Ana sayfa
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
  });

// Diğer route'lar
app.get('/about', (req, res) => {
  res.send('Hakkımızda sayfası');
});

app.get('/contact', (req, res) => {
  res.send('İletişim sayfası');
});

// Sunucuyu dinle
app.listen(3000, () => {
  console.log('Sunucu çalışıyor...');
});